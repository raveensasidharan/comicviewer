package com.example.comicproject;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class ComicAppController extends Application
{
    private static ComicAppController sInstance;
    private RequestQueue mRequestQueue;
    @Override
    public void onCreate()
    {
        super.onCreate();

    }

    public static ComicAppController getInstance()
    {
        if(sInstance==null)
        {
            sInstance=new ComicAppController();
        }
        return sInstance;

    }


    public RequestQueue getmRequestQueue(Context context)
    {
        if(null == mRequestQueue)
        {
            mRequestQueue = Volley.newRequestQueue(context);
        }

        return mRequestQueue;
    }

}
