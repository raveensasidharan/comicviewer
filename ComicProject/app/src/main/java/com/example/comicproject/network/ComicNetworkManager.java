package com.example.comicproject.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.comicproject.ComicAppController;
import com.example.comicproject.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ComicNetworkManager {
    private final static String TAG  = ComicNetworkManager.class.getSimpleName();

    public interface ComicNetworkManagerListener
    {
        void onRequestStatusReceived(ComicNetworkStatus statusCode, String response);
    }

    private ComicNetworkManagerListener mListener;
    private Context mContext;

    public ComicNetworkManager(Context context)
    {
        mContext = context;
    }

    public void makeJSonObjectRequest(int method, String url, JSONObject reqObject, final ComicNetworkManagerListener listener)
    {
        // mListener = listener;

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(method,url, reqObject,
                new Response.Listener<JSONObject>()
                {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        Log.d(TAG, response.toString());
                        listener.onRequestStatusReceived(ComicNetworkStatus.OK,response.toString());

                    }
                },
                new Response.ErrorListener()
                {

                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Log.e("signup","error is "+error.getMessage());
                        listener.onRequestStatusReceived(ComicNetworkStatus.ERROR, null);

                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");

                return headers;
            }
        };

        //set retry policy
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);

        RequestQueue requestQueue = ComicAppController.getInstance().getmRequestQueue(mContext);
        requestQueue.add(jsonObjReq);

    }

}
