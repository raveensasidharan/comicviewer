package com.example.comicproject.model;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
public class ComicResponseModel
{
    private int mNumber;
    private int mDate;
    private int mMonth;
    private int mYear;

    private String mTitle;
    private String mSafeTitle;
    private String mNews;
    private String mLink;
    private String mAlt;
    private String mTranscript;
    private String mImageLink;

    public ComicResponseModel(JSONObject comicJSON)
    {
        mNumber = comicJSON.optInt("num");
        mDate = comicJSON.optInt("day");
        mMonth = comicJSON.optInt("month");
        mYear = comicJSON.optInt("year");

        mTitle  = comicJSON.optString("title");
        mSafeTitle  = comicJSON.optString("safe_title");
        mNews  = comicJSON.optString("news");
        mLink  = comicJSON.optString("link");
        mImageLink  = comicJSON.optString("img");
        mAlt  = comicJSON.optString("alt");
        mTranscript  = comicJSON.optString("transcript");
    }

}


