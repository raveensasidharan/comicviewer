package com.example.comicproject.manager;

import android.content.Context;
import android.graphics.Camera;

import com.example.comicproject.model.ComicResponseModel;
import com.example.comicproject.network.ComicNetworkManager;
import com.example.comicproject.network.ComicNetworkStatus;

import org.json.JSONException;
import org.json.JSONObject;

import static com.android.volley.Request.Method.GET;

public class ComicManager
{
    private Context mContext;
    private ComicNetworkManager mNetworkManager;


    public interface IComicManagerListener
    {
        void onComicItemReceived(ComicResponseModel comicResponseModel);
    }


    public ComicManager(Context context)
    {
        this.mContext   = context;
        mNetworkManager = new ComicNetworkManager(mContext);
    }

    public void requestComicItem(int comicNumber, final IComicManagerListener listener)
    {
        mNetworkManager.makeJSonObjectRequest(GET, null, null, new ComicNetworkManager.ComicNetworkManagerListener() {
            @Override
            public void onRequestStatusReceived(ComicNetworkStatus statusCode, String response) {
                if (ComicNetworkStatus.OK == statusCode)
                {
                    try
                    {
                        JSONObject responseJSON = new JSONObject(response);
                        ComicResponseModel comicResponseModel   = new ComicResponseModel(responseJSON);
                        listener.onComicItemReceived(comicResponseModel);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

}
